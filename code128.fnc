CREATE OR REPLACE FUNCTION code128(p_input VARCHAR2, p_check INTEGER := 0)
	RETURN VARCHAR2 DETERMINISTIC IS
	/*
   *  This function is governed by the GNU Lesser General Public License (GNU LGPL)
   *  Parameters : a string
   *  Return : * a string which give the bar code when it is dispayed with CODE128.TTF font
   *           * an empty string if the supplied parameter is no good
   *
   * written by David Pyke (eselle@sourceforge.net)
   * modified for use with UTF-8 encoding by Dmitry Bogomolov (Dmitry.Bogomolov@gmail.com)
   *
   * based on code found at http://grandzebu.net/informatique/codbar-en/codbar.htm
   *                        http://sourceforge.net/projects/openbarcodes
   *                        https://sourceforge.net/p/openbarcodes/discussion/417149/thread/33264316/#8f9c
   *
  */
	i      NUMBER;
	chksum NUMBER;
	mini   NUMBER;
	dummy  NUMBER;
	tableb BOOLEAN;
	c_pinput_length CONSTANT NUMBER := length(p_input);
	v_retval VARCHAR2(100);
	s        VARCHAR2(20);

	FUNCTION usetablec(p_start NUMBER, p_cnt NUMBER) RETURN BOOLEAN IS
		-- Determine if the p_cnt characters from p_start are numeric
	BEGIN
		FOR x IN p_start .. p_start + p_cnt LOOP
		
			IF (x > c_pinput_length) THEN
				RETURN FALSE;
			END IF;
			IF ascii(substr(p_input, x, 1)) < 48 OR ascii(substr(p_input, x, 1)) > 57 THEN
				RETURN FALSE;
			END IF;
		END LOOP;
	
		RETURN TRUE;
	END usetablec;
BEGIN
	IF c_pinput_length = 0 THEN
		RETURN NULL;
	END IF;

	-- Checking for invalid characters
	IF p_check = 1 THEN
		FOR i IN 1 .. length(p_input) LOOP
			IF NOT (ascii(substr(p_input, i, 1)) >= 21 AND ascii(substr(p_input, i, 1)) <= 126 OR
					ascii(substr(p_input, i, 1)) = 203) THEN
				raise_application_error(-20001,
																'P_INPUT contains incorrect symbol #' || i ||
																' ("' || substr(p_input, i, 1) || '")');
			END IF;
		END LOOP;
	END IF;

	tableb := TRUE;
	i      := 1;

	FOR c IN 1 .. c_pinput_length LOOP
		EXIT WHEN i > c_pinput_length;
		IF tableb THEN
			IF i = 1 OR (i + 3) = length(p_input) THEN
				mini := 4;
			ELSE
				mini := 6;
			END IF;
			IF usetablec(i, mini) THEN
				IF i = 1 THEN
					v_retval := unistr('\00CD'); --start with tableC
				ELSE
					v_retval := v_retval || unistr('\00C7'); --switch to tabelC
				END IF;
				tableb := FALSE;
			ELSE
				IF i = 1 THEN
					v_retval := unistr('\00CC');
				END IF; --Starting with table B
			END IF;
		END IF;
		IF NOT tableb THEN
			--We are on table C, try to process 2 digits
			mini := 2;
			IF usetablec(i, mini) THEN
				dummy := to_number(substr(p_input, i, 2));
				IF (dummy < 95) THEN
					dummy := dummy + 32;
				ELSE
					dummy := dummy + 100;
				END IF;
				v_retval := v_retval || chr(dummy);
				i        := i + 2;
			ELSE
				v_retval := v_retval || unistr('\00C8');
				tableb   := TRUE;
			END IF;
		END IF;
		IF tableb THEN
			--Process 1 digit with table B
			v_retval := v_retval || substr(p_input, i, 1);
			i        := i + 1;
		END IF;
	END LOOP;
  
	--Calculation of the checksum
	FOR i IN 1 .. length(v_retval) LOOP
		s := substr(v_retval, i, 1);
		IF (s = asciistr(s)) THEN
			dummy := ascii(s);
		ELSE
			s     := asciistr(s);
			s     := REPLACE(s, '\');
			dummy := to_number(s, 'fmXXXX');
		END IF;
	
		IF (dummy < 127) THEN
			dummy := dummy - 32;
		ELSE
			dummy := dummy - 100;
		END IF;
		IF i = 1 THEN
			chksum := dummy;
		END IF;
	
		chksum := MOD((chksum + (i - 1) * dummy), 103);
	END LOOP;

	-- Calculation of the checksum ASCII code
	IF chksum < 95 THEN
		chksum := chksum + 32;
	ELSE
		chksum := chksum + 100;
	END IF;

	--Add the checksum and the STOP codes
	v_retval := v_retval || unistr('\' || to_char(chksum, 'fm0XXX')) || unistr('\00CE');
  
	RETURN v_retval;
END;
/
